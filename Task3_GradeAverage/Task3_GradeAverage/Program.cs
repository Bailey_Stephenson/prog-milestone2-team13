﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3_GradeAverage
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Hello");
            Console.WriteLine("Are You Level 5 or Level 6 ?");
            Console.WriteLine("Enter 5 or 6:");

            var lvlchoice = Console.ReadLine();
            var studentid = 0;
            switch (lvlchoice)
            {
                case "5":
                    var papercode1 = "";
                    int papercode1mark = 0;
                    var papercode2 = "";
                    int papercode2mark = 0;
                    var papercode3 = "";
                    int papercode3mark = 0;
                    var papercode4 = "";
                    int papercode4mark = 0;
                    const int totalpapers5 = 4;

                    Console.WriteLine("Please enter student id: ");
                    studentid = int.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter paper code 1: ");
                    papercode1 = (Console.ReadLine());
                    Console.WriteLine("Mark for paper code 1: ");
                    papercode1mark = int.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter paper code 2: ");
                    papercode2 = (Console.ReadLine());
                    Console.WriteLine("Mark for paper code 2: ");
                    papercode2mark = int.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter paper code 3: ");
                    papercode3 = (Console.ReadLine());
                    Console.WriteLine("Mark for paper code 3: ");
                    papercode3mark = int.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter paper code 4: ");
                    papercode4 = (Console.ReadLine());
                    Console.WriteLine("Mark for paper code 4: ");
                    papercode4mark = int.Parse(Console.ReadLine());
                    Console.Clear();

                    totals5(papercode1, papercode2, papercode3, papercode4, papercode1mark, papercode2mark, papercode3mark, papercode4mark, studentid, lvlchoice);

                    gradeaverage5(papercode1mark, papercode2mark, papercode3mark, papercode4mark, totalpapers5);

                    break;

                case "6":
                    var papercode1_1 = "";
                    int papercode1mark_1 = 0;
                    var papercode2_1 = "";
                    int papercode2mark_1 = 0;
                    var papercode3_1 = "";
                    int papercode3mark_1 = 0;
                    const int totalpapers6 = 3;

                    Console.WriteLine("Please enter student id: ");
                    studentid = int.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter paper code 1: ");
                    papercode1_1 = (Console.ReadLine());
                    Console.WriteLine("Mark for paper code 1: ");
                    papercode1mark_1 = int.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter paper code 2: ");
                    papercode2_1 = (Console.ReadLine());
                    Console.WriteLine("Mark for paper code 2: ");
                    papercode2mark_1 = int.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter paper code 3: ");
                    papercode3_1 = (Console.ReadLine());
                    Console.WriteLine("Mark for paper code 3: ");
                    papercode3mark_1 = int.Parse(Console.ReadLine());
                    Console.Clear();

                    totals6(studentid, papercode1_1, papercode1mark_1, papercode2_1, papercode2mark_1, papercode3_1, papercode3mark_1, lvlchoice);

                    gradeaverage6(papercode1mark_1, papercode2mark_1, papercode3mark_1, totalpapers6);
                    break;
            }



        }
        static void totals5(string papercode1, string papercode2, string papercode3, string papercode4, int papercode1mark, int papercode2mark, int papercode3mark, int papercode4mark, int lvlchoice, string studentid)
        {
            Console.WriteLine($"Level {studentid}, {lvlchoice}");
            Console.WriteLine($"Your PAPERS are: {papercode1}, {papercode2}, {papercode3}, {papercode4}");
            Console.WriteLine($"Your MARKS are: {papercode1mark}, {papercode2mark}, {papercode3mark}, {papercode4mark} "); 

        }
        static void totals6(int studentid, string papercode1_1, int papercode1mark_1, string papercode2_1, int papercode2mark_1, string papercode3_1, int papercode3mark_1, string lvlchoice)
        {
            Console.WriteLine($"Level {lvlchoice}, {studentid}");
            Console.WriteLine($"Your PAPERS are: {papercode1_1}, {papercode2_1}, {papercode3_1}");
            Console.WriteLine($"Your MARKS are: {papercode1mark_1}, {papercode2mark_1}, {papercode3mark_1}");
        }
        static void gradeaverage5(int papercode1mark, int papercode2mark, int papercode3mark, int papercode4mark, int totalpapers5)
        {
            Console.WriteLine($"Your Grade Average is: {(papercode1mark + papercode2mark + papercode3mark + papercode4mark) / totalpapers5}");
        }
        static void gradeaverage6(int papercode1mark_1, int papercode2mark_1, int papercode3mark_1, int totalpapers6)
        {
            Console.WriteLine($"Your Grade Average is: {(papercode1mark_1 + papercode2mark_1 + papercode3mark_1) / totalpapers6}");
        }
    }
}
